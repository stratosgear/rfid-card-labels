from reportlab.graphics import renderPDF
from reportlab.graphics.shapes import Drawing, Image, Line, Rect, String
from reportlab.lib.pagesizes import A4, letter, landscape
from reportlab.lib.units import mm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.pdfmetrics import stringWidth
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen.canvas import Canvas
import csv


# Can fit 3 x 3 in a A4 page
# Three horizontal cards: 52 * 3 = 156mm
# Three vertical cards: 83.5 * 3 = 250.5mm
# Which is within the 210 x 297 mm of an A4


# the intention is to create labels that can be printed with the album cover on
# top and the image to bleed perfectly on the edge of the label

# Each label should include the following features:

#    % |               | %  <- Crop marks
#    %+-----------------+%
# --  |                 | -- <- Crop marks
#     |                 |
#     |    Album        |
#     |      Cover      |<- Square image
#     |         Image   |
#     |                 |
#     +-----------------+
#         Album Title        }
#         Artist Name        }-> White area, with textual description
#            Genre           }
# --                      --
#      |               |
#
# %: BLEED_EXTEND mm safety margin where the image extends the label area,
# so bleeds appear nice.

# page info
PAGESIZE = A4
LANDSCAPE = False

# RFID Card size (Already applied a 1mm safety margin around card)
CARDSIZE = (52 * mm, 83.5 * mm)

# Manually do some math on how many labels fit in the page
NUM_LABELS_X = 3
NUM_LABELS_Y = 3

# crop marks info
CROPMARK_LEN = 2 * mm
CROPMARK_BRD = 1 * mm

# image info
BLEED_EXTEND = 0.5 * mm

# text info
TITLE_POS = 25 * mm
TITLE_FONT = "Vera"
TITLE_FONT_SIZE = 14

ARTIST_POS = 20 * mm
ARTIST_FONT = "Vera-bold"
ARTIST_FONT_SIZE = 15

GENRE_POS = 12 * mm
GENRE_FONT = "Vera-italic"
GENRE_FONT_SIZE = 10

# Page borders
LR_BORDER = 10 * mm
TB_BORDER = 10 * mm

# nothing to edit below

page_width = landscape(PAGESIZE)[0] if LANDSCAPE else PAGESIZE[0]
page_height = landscape(PAGESIZE)[1] if LANDSCAPE else PAGESIZE[1]

LABEL_WIDTH = (page_width - 2 * LR_BORDER) / NUM_LABELS_X
LABEL_HEIGHT = (page_height - 2 * TB_BORDER) / NUM_LABELS_Y
SHEET_TOP = page_height

pdfmetrics.registerFont(TTFont("Vera", "Vera.ttf"))
pdfmetrics.registerFont(TTFont("Vera-bold", "VeraBd.ttf"))
pdfmetrics.registerFont(TTFont("Vera-italic", "VeraIt.ttf"))


# Wrapping multiline text in ReportLab is a bitch!
# Fit long strings in one line.
def fit_one_line(txt: str, width: float, initFontSize: int, font: str) -> int:
    fontSize = initFontSize
    while stringWidth(txt, font, fontSize) > width:
        fontSize = fontSize - 1
    return fontSize


def label(cover: str, albumName: str, artistName: str, genres: str) -> Drawing:

    label_drawing = Drawing(LABEL_WIDTH, LABEL_HEIGHT)

    # Card corner points
    bl_x = CROPMARK_LEN + CROPMARK_BRD
    bl_y = CROPMARK_LEN + CROPMARK_BRD

    br_x = bl_x + CARDSIZE[0]
    br_y = bl_y

    tl_x = CROPMARK_LEN + CROPMARK_BRD
    tl_y = bl_y + CARDSIZE[1]

    tr_x = br_x
    tr_y = tl_y

    # Card middle point
    mx = bl_x + CARDSIZE[0] / 2
    my = bl_x + CARDSIZE[1] / 2

    # albumName = "Album Title"
    fontSize = fit_one_line(
        albumName, CARDSIZE[0] - BLEED_EXTEND * 4, TITLE_FONT_SIZE, TITLE_FONT
    )
    title = String(
        mx,
        TITLE_POS,
        albumName,
        fontName=TITLE_FONT,
        fontSize=fontSize,
        textAnchor="middle",
    )
    label_drawing.add(title)

    # artistName = "Artist"
    fontSize = fit_one_line(
        artistName, CARDSIZE[0] - BLEED_EXTEND * 4, ARTIST_FONT_SIZE, ARTIST_FONT
    )
    artist = String(
        mx,
        ARTIST_POS,
        artistName,
        fontName=ARTIST_FONT,
        fontSize=fontSize,
        textAnchor="middle",
    )
    label_drawing.add(artist)

    # genres = "(Genre, other genre)"
    fontSize = fit_one_line(
        f"({genres})", CARDSIZE[0] - BLEED_EXTEND * 4, GENRE_FONT_SIZE, GENRE_FONT
    )
    artist = String(
        mx,
        GENRE_POS,
        f"({genres})",
        fontName=GENRE_FONT,
        fontSize=fontSize,
        textAnchor="middle",
    )
    label_drawing.add(artist)

    img = Image(
        tl_x - BLEED_EXTEND,
        tl_y - CARDSIZE[0] - BLEED_EXTEND,
        CARDSIZE[0] + 2 * BLEED_EXTEND,
        CARDSIZE[0] + 2 * BLEED_EXTEND,
        # tr_x + BLEED_EXTEND,
        # tr_y + BLEED_EXTEND,
        "cover.jpg",
    )
    label_drawing.add(img)

    # Crop marks bottom left
    cr_vbl = Line(bl_x, 0, bl_x, CROPMARK_LEN, strokeWidth=0.4)
    label_drawing.add(cr_vbl)

    cr_hbl = Line(0, bl_y, CROPMARK_LEN, bl_y, strokeWidth=0.4)
    label_drawing.add(cr_hbl)

    # Crop marks bottom right
    cr_vbr = Line(br_x, 0, br_x, CROPMARK_LEN, strokeWidth=0.4)
    label_drawing.add(cr_vbr)

    cr_hbl = Line(
        br_x + CROPMARK_LEN + CROPMARK_BRD,
        bl_y,
        br_x + CROPMARK_BRD,
        bl_y,
        strokeWidth=0.4,
    )
    label_drawing.add(cr_hbl)

    # Crop marks top left
    cr_vtl = Line(
        tl_x,
        tl_y + CROPMARK_LEN + CROPMARK_BRD,
        tl_x,
        tl_y + CROPMARK_BRD,
        strokeWidth=0.4,
    )
    label_drawing.add(cr_vtl)

    cr_htl = Line(
        tl_x - CROPMARK_LEN - CROPMARK_BRD,
        tl_y,
        tl_x - CROPMARK_BRD,
        tl_y,
        strokeWidth=0.4,
    )
    label_drawing.add(cr_htl)

    # Crop marks top right
    cr_vtr = Line(
        tr_x + CROPMARK_LEN + CROPMARK_BRD,
        tr_y,
        tr_x + CROPMARK_BRD,
        tr_y,
        strokeWidth=0.4,
    )
    label_drawing.add(cr_vtr)

    cr_htr = Line(
        tr_x,
        tr_y + CROPMARK_LEN + CROPMARK_BRD,
        tr_x,
        tr_y + CROPMARK_BRD,
        strokeWidth=0.4,
    )
    label_drawing.add(cr_htr)

    return label_drawing


def fill_sheet(canvas: Canvas):

    with open("data.csv", newline="") as data:
        csv_reader = csv.reader(data, doublequote=True)

        for yl in range(0, NUM_LABELS_Y):
            for xl in range(0, NUM_LABELS_X):
                # print(next(csv_reader))
                d = next(csv_reader)
                aLabel = label(d[0], d[1], d[2], d[3])
                x = LR_BORDER + xl * LABEL_WIDTH
                y = SHEET_TOP - TB_BORDER - (yl + 1) * LABEL_HEIGHT
                renderPDF.draw(aLabel, canvas, x, y)


if __name__ == "__main__":

    canvas = (
        Canvas("rfid-stickers.pdf", pagesize=landscape(PAGESIZE))
        if LANDSCAPE
        else Canvas("rfid-stickers.pdf", pagesize=PAGESIZE)
    )
    fill_sheet(canvas)
    canvas.save()
